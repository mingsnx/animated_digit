## [2.0.4] - null-safety Version. resolve text textScaleFactor
* 2.0.4 release
* ✅ null-safety Version!.
* ✅ resolve textScaleFactor
* ✅ 解决设备字体因 textScaleFactor 改变的影响
* + duration field
## [2.0.2] - null-safety Version. Fix the effect of digitSplitSymbol after negative numbers
* 2.0.2 release
* ✅null-safety Version!.
* ✅Fix the effect of digitSplitSymbol after negative numbers
* ✅修复负数后digitSplitSymbol的影响
## [2.0.1] - not null-safety Version. Fix the effect of digitSplitSymbol after negative numbers
* 2.0.1 release
* ✅not null-safety Version!.
* ✅Fix the effect of digitSplitSymbol after negative numbers
* ✅修复负数后digitSplitSymbol的影响

## [2.0.0] - Migrate to null-safety.
* 2.0.0 release
* ✅Migrate to null-safety.
* ✅迁移至空安全

## [1.0.6] - Fix BUG Cannot scroll after calling'addValue' or'resetValue' for the second time
* 1.0.6 release
* ✅Cannot scroll after calling'addValue' or'resetValue' for the second time
* ✅第二次调用“ addValue”或“ resetValue”后无法滚动

## [1.0.5+1] - property digitSplitSymbol name modify error
* 1.0.5+1 release
* (property modify error) <= 1.0.4 digitSplitSymbol => (1.0.5)❌digitSplitNumber => (now)✅digitSplitSymbol 

## [1.0.5] - Prevent ScrollController not attached to any scroll views
* 1.0.5 release
* Add assert、annotation
* Part param default handler
* Prevent ScrollController not attached to any scroll views

## [1.0.4] - Fix that integers are not supported
* 1.0.4 release
* 修复支持整数

## [1.0.3] - optimization
* 1.0.3 release
* 区分分隔字符和数字的Size计算

## [1.0.2] - solve digital precision
* 1.0.2 release. 
* solve digital precision
* 引入 number_precision 解决数字精度git add .

## [1.0.1] - reset bug
* 1.0.1 release. 
* reset bug

## [1.0.0] - first publish.
* 1.0.0 release.